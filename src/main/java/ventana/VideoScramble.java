package ventana;



import java.util.Random;

public class VideoScramble {

    Random r = new Random(System.currentTimeMillis());
    Random r2 = new Random(System.currentTimeMillis());
    Random r3 = new Random(System.currentTimeMillis());
    Random r4 = new Random(System.currentTimeMillis());
    String[] moves = {"Rx", "Lx", "Uy", "Dy", "Fz", "Bz"};
    String[] direccion = {" ", "' ", "2 "};
    String[] moves2 = {"Rx", "Uy", "Fz"};
    String[] direccion2 = {" ", "' ", "2 "};
    String[] moves3 = {"Rx", "Lx", "Uy", "Dy", "Fz", "Bz"};
    String[] direccion3 = {" ", "' ", "2 ", "w ", "w2 ", "w' "};
    String[] moves4 = {"Rx", "Lx", "Uy", "Dy", "Fz", "Bz"};
    String[] direccion4 = {" ", "' ", "2 ", "w ", "w2 ", "w' "};
    String[] moves5 = {"Rx","Dy"};
    String[] direccion5 = {"++ ","-- "};
    String[] moves6 = {"Rx","Lx","Uy","Bz","rx","lx","uy","bz"};
    String[] direccion6 = {" ", "' "};
     String[] moves7 = {"Rx","Lx","Uy","Bz"};
    String[] direccion7 = {" ", "' "};
    String[] moves8 = {"Ux","Dy"};
    String[] direccion8 = {"R1- ","L1- ","R2- ","L2- ","L3- ","R3- ","R1+ ","L1+ ","R2+ ","L2+ ","L3+ ","R3+ ","R4- ","L4- ","R5- ","L5- ","L6- ","R6- ","R4+ ","L4+ ","R5+ ","L5+ ","L6+ ","R6+ "};
    String[] moves9 ={"Rx","Lx","Uy","Dy"};
    String[] direccion9 ={"1- ","2- ","3- ","4- ","5- ","6- ","1+ ","2+ ","3+ ","4+ ","5+ ","6+ "};
    String[] moves10 ={"Ux","Dy"};
    String[] direccion10={"R ","L "};
    String[] moves11 ={"Ax"};
    String[] direccion11={"LL1- ","LL2- ","LL3- ","LL4- ","LL5- ","LL6+ ","LL1+ ","LL2+ ","LL3+ ","LL4+ ","LL5+ ","LL6+ "};
    String[] moves12={};
    String[] direccion12={};

    public static void main(String... rubiks) {
        VideoScramble cube = new VideoScramble();
        cube.getScramble();
    }

    public String getScramble() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 25; i++) {
            String currentMove = getMove2(move1, move2);
            direccio = direccion[r.nextInt(direccion.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        return scramble;


    }

    public String getScramble2x2() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 10; i++) {
            String currentMove = getMove2(move1, move2);
            direccio = direccion2[r2.nextInt(direccion2.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        return scramble;
        

    }
    public void delaySegundo() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

    }
    public void delaySegundo2() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }

    }

    public String getScramble4x4() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 22; i++) {
            String currentMove = getMove3(move1, move2);
            direccio = direccion3[r3.nextInt(direccion3.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
       return scramble;


    }

    public String getScramble5x5() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 21; i++) {
            String currentMove = getMove3(move1, move2);
            direccio = direccion4[r4.nextInt(direccion4.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
      return scramble;


    }
     public String getScramblemega() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 12; i++) {
            String currentMove = getMove5(move1, move2);
            direccio = direccion5[r4.nextInt(direccion5.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
      return scramble;


    }
         public String getScramblepyra() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 9; i++) {
            String currentMove = getMove6(move1, move2);
            direccio = direccion6[r2.nextInt(direccion6.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        return scramble;
        

    }
   public String getScramblesq1() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 9; i++) {
            String currentMove = getMove7(move1, move2);
            direccio = direccion7[r2.nextInt(direccion7.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        return scramble;
        

    }
      public String getScrambleclock() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 1; i++) {
            String currentMove = getMove9(move1, move2);
            direccio = direccion9[r2.nextInt(direccion9.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        return scramble;
        

    }
   public String getScrambleclockdos() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 3; i++) {
            String currentMove = getMove8(move1, move2);
            direccio = direccion8[r2.nextInt(direccion8.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        return scramble;
        

    }  
     public String getScrambleclocktres() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 3; i++) {
            String currentMove = getMove10(move1, move2);
            direccio = direccion10[r2.nextInt(direccion10.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        return scramble;
        

    }
        public String getScrambleclockall() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 1; i++) {
            String currentMove = getMove11(move1, move2);
            direccio = direccion11[r2.nextInt(direccion11.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        return scramble;
        

    }

    public String getMove(String m1, String m2) {
        String move = moves[r.nextInt(moves.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove(m1, m2);

        }

        return move;
    }

    public String getMove2(String m1, String m2) {
        String move = moves2[r2.nextInt(moves2.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove2(m1, m2);

        }

        return move;
    }

    public String getMove3(String m1, String m2) {
        String move = moves3[r3.nextInt(moves3.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove3(m1, m2);

        }

        return move;
    }

    public String getMove4(String m1, String m2) {
        String move = moves4[r4.nextInt(moves4.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove4(m1, m2);

        }

        return move;
    }
      public String getMove5(String m1, String m2) {
        String move = moves5[r4.nextInt(moves5.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove5(m1, m2);

        }

        return move;
    }
          public String getMove6(String m1, String m2) {
        String move = moves6[r4.nextInt(moves6.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove6(m1, m2);

        }

        return move;
    }
  public String getMove7(String m1, String m2) {
        String move = moves7[r4.nextInt(moves7.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove7(m1, m2);

        }

        return move;
    }
      public String getMove8(String m1, String m2) {
        String move = moves8[r.nextInt(moves8.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove8(m1, m2);

        }

        return move;
    }
    public String getMove9(String m1, String m2) {
        String move = moves9[r.nextInt(moves9.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove9(m1, m2);

        }

        return move;
    }
        public String getMove10(String m1, String m2) {
        String move = moves10[r.nextInt(moves10.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove10(m1, m2);

        }

        return move;
    }
          public String getMove11(String m1, String m2) {
        String move = moves11[r.nextInt(moves11.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove11(m1, m2);

        }

        return move;
    }
    
    public boolean repetit(String m1, String m2, String m3) {
        if (m2.charAt(1) == m1.charAt(1) && m2.charAt(1) == m3.charAt(1)) {
            return true;

        }
        return false;
    }
}
